import { assertObjectMatch } from "https://deno.land/std@0.217.0/assert/assert_object_match.ts"
import { flatten } from "../../util/flatten.ts"

Deno.test("flatten() to plain objects", () => {
  assertObjectMatch(
    flatten({ foo: "bar", test: true }),
    {
      foo: "bar",
      test: true,
    },
  )
})

Deno.test("flatten() to handle arrays", () => {
  assertObjectMatch(
    flatten({ foo: [0, 1, 2] }),
    {
      "foo[0]": 0,
      "foo[1]": 1,
      "foo[2]": 2,
    },
  )
})

Deno.test("flatten() to handle objects", () => {
  assertObjectMatch(
    flatten({ foo: { bar: 1, baz: false } }),
    {
      "foo[bar]": 1,
      "foo[baz]": false,
    },
  )
})

Deno.test("flatten() to handle deeper nest", () => {
  const flat = flatten({ foo: { bar: { baz: { bor: "box" } } } })

  assertObjectMatch(
    flat,
    {
      "foo[bar][baz][bor]": "box",
    },
    JSON.stringify(flat),
  )
})

Deno.test("flatten() to handle mixed nest", () => {
  const flat = flatten({
    foo: {
      bar: {
        baz: { bor: "box" },
        test: ["zero", "one"],
        another: false,
      },
    },
  })

  assertObjectMatch(
    flat,
    {
      "foo[bar][baz][bor]": "box",
      "foo[bar][test][0]": "zero",
      "foo[bar][test][1]": "one",
      "foo[bar][another]": false,
    },
    JSON.stringify(flat),
  )
})
