import { assertEquals } from "https://deno.land/std@0.217.0/assert/assert_equals.ts"
import { hmacSha256 } from "../../hash/hmacSha256.ts"

Deno.test("hmacSha256()", async () => {
  assertEquals(
    await hmacSha256("key", "data"),
    "UDH+PZicbRU3oBP6bnOdojRj/a7DtwE32Cjjas4iG9A=",
  )
})
