import type { MagentoApiOptions } from "../../Magento2Api.ts"
import { load } from "https://deno.land/std@0.218.2/dotenv/mod.ts"

export async function loadConfig(): Promise<MagentoApiOptions> {
  await load({ export: true })

  const url = Deno.env.get("M2_TEST_URL")

  if (!url) {
    throw new Error("Test server not configured")
  }

  return {
    url,
    consumerKey: Deno.env.get("M2_TEST_CONSUMER_KEY"),
    consumerSecret: Deno.env.get("M2_TEST_CONSUMER_SECRET"),
    accessToken: Deno.env.get("M2_TEST_ACCESS_TOKEN"),
    tokenSecret: Deno.env.get("M2_TEST_TOKEN_SECRET"),
  }
}
