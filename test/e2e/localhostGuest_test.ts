import { assertEquals } from "https://deno.land/std@0.217.0/assert/assert_equals.ts"
import { Magento2Api } from "../../Magento2Api.ts"
import { loadConfig } from "./loadConfig.ts"

Deno.test("localhost - guest", async () => {
  let config
  try {
    config = await loadConfig()
  } catch (e) {
    console.log(`skipped - ${e}`)
    return
  }

  console.log(config.url)
  const magento = new Magento2Api({
    url: config.url,
  })

  const response = await magento.request("GET", "directory/countries")

  assertEquals(response.status, 200)
  console.log(await response.json())
})
