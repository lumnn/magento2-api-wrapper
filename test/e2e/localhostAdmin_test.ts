import { assertEquals } from "https://deno.land/std@0.217.0/assert/assert_equals.ts"
import { Magento2Api } from "../../Magento2Api.ts"
import { loadConfig } from "./loadConfig.ts"

Deno.test("localhost - admin", async (t) => {
  let config
  try {
    config = await loadConfig()
  } catch (e) {
    console.log(`skipped - ${e}`)
    return
  }

  console.log(config.url)
  const magento = new Magento2Api({
    ...config,
  })

  await t.step("GETs without params", async () => {
    const response = await magento.request("GET", "store/storeConfigs")

    console.log(await response.json())
    assertEquals(response.status, 200)
  })

  await t.step("GETs with params", async () => {
    const response = await magento.request("GET", "products", null, {
      params: {
        searchCriteria: {
          currentPage: 1,
          pageSize: 1,
        },
      },
    })

    console.log(await response.json())
    assertEquals(response.status, 200)
  })
})
