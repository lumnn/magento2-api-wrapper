import { assertEquals } from "https://deno.land/std@0.217.0/assert/mod.ts"
import { OAuth } from "../OAuth.ts"

Deno.test("authorize()", async (t) => {
  const consumer = {
    key: "1434affd-4d69-4a1a-bace-cc5c6fe493bc",
    secret: "932a216f-fb94-43b6-a2d2-e9c6b345cbea",
  }
  const oauth = new OAuth({ consumer })

  // for consistent test results
  const timestamp = "1318622958"
  oauth.timestamp = function () {
    return timestamp
  }

  // for consistent test results
  const nonce = "kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg"
  oauth.nonce = function () {
    return nonce
  }

  const token = {
    key: "370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb",
    secret: "LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE",
  }

  const request = {
    url: "https://api.twitter.com/1/statuses/update.json?include_entities=true",
    method: "POST",
  }

  await t.step("without realm", async () => {
    assertEquals(
      await oauth.authorize(request, token),
      `OAuth ` + [
        `oauth_consumer_key="${consumer.key}"`,
        `oauth_nonce="${nonce}"`,
        `oauth_signature="${consumer.secret}%26${token.secret}"`,
        `oauth_signature_method="PLAINTEXT"`,
        `oauth_timestamp="${timestamp}"`,
        `oauth_token="${token.key}"`,
        `oauth_version="1.0"`,
      ].join(","),
    )
  })

  await t.step("with realm", async () => {
    oauth.realm = "https://example.org"

    assertEquals(
      await oauth.authorize(request, token),
      `OAuth ` + [
        `realm="${oauth.realm}"`,
        `oauth_consumer_key="${consumer.key}"`,
        `oauth_nonce="${nonce}"`,
        `oauth_signature="${consumer.secret}%26${token.secret}"`,
        `oauth_signature_method="PLAINTEXT"`,
        `oauth_timestamp="${timestamp}"`,
        `oauth_token="${token.key}"`,
        `oauth_version="1.0"`,
      ].join(","),
    )
  })
})

Deno.test("authorize() 2", async () => {
  const consumer = {
    key: "abcd",
    secret: "efgh",
  }

  const oauth = new OAuth({
    consumer,
    signatureMethod: "PLAINTEXT",
  })

  // for consistent test results
  const timestamp = "1462028868"
  oauth.timestamp = function () {
    return timestamp
  }

  // for consistent test results
  const nonce = "FDRMnsTvyF1"
  oauth.nonce = function () {
    return nonce
  }

  const token = {
    key: "ijkl",
    secret: "mnop",
  }

  const request = {
    url: "http://host.net/resource",
    method: "GET",
  }

  assertEquals(
    await oauth.authorize(request, token),
    `OAuth oauth_consumer_key="abcd",oauth_nonce="FDRMnsTvyF1",oauth_signature="efgh%26mnop",oauth_signature_method="PLAINTEXT",oauth_timestamp="1462028868",oauth_token="ijkl",oauth_version="1.0"`,
  )
})

Deno.test("nonce() generates random string", () => {
  const oauth = new OAuth({
    consumer: { key: "test", secret: "test" },
  })

  const nonce = oauth.nonce()
  console.log(nonce)
  assertEquals(nonce.length, 32, "length to be 32 chars by default")
})

Deno.test("nonce() size can be changed", () => {
  const oauth = new OAuth({
    consumer: { key: "test", secret: "test" },
    nonceLength: 16,
  })

  const nonce = oauth.nonce()
  console.log(nonce)
  assertEquals(nonce.length, 16, "length to be 32 chars by default")
})

const oauthSignatureTest = new OAuth({ consumer: { key: "abcd", secret: "efgh" }})

const oauthData = {
  oauth_consumer_key: "abcd",
  oauth_nonce: "6MDdBa31nmI",
  oauth_signature_method: "PLAINTEXT",
  oauth_timestamp: "1462028641",
  oauth_token: "ijkl",
  oauth_version: "1.0"
}

Deno.test("getSignatureBaseString() to build valid signature string", async () => {
  const signatureBase = await oauthSignatureTest.getSignatureBaseString({
    url: 'http://host.net/resource?test[1]=false',
    method: 'GET'
  }, oauthData)

  assertEquals(signatureBase, "GET&http%3A%2F%2Fhost.net%2Fresource&oauth_consumer_key%3Dabcd%26oauth_nonce%3D6MDdBa31nmI%26oauth_signature_method%3DPLAINTEXT%26oauth_timestamp%3D1462028641%26oauth_token%3Dijkl%26oauth_version%3D1.0%26test%255B1%255D%3Dfalse")
})

Deno.test("getSignatureBaseString() to build valid signature string when hash passed", async () => {
  const signatureBase = await oauthSignatureTest.getSignatureBaseString({
    url: 'http://host.net/resource?test[1]=false#hash',
    method: 'GET'
  }, oauthData)

  assertEquals(signatureBase, "GET&http%3A%2F%2Fhost.net%2Fresource&oauth_consumer_key%3Dabcd%26oauth_nonce%3D6MDdBa31nmI%26oauth_signature_method%3DPLAINTEXT%26oauth_timestamp%3D1462028641%26oauth_token%3Dijkl%26oauth_version%3D1.0%26test%255B1%255D%3Dfalse")
})

Deno.test("getSignatureBaseString() to build valid signature string with query string passed with containing encoded params", async () => {
  const signatureBase = await oauthSignatureTest.getSignatureBaseString({
    url: 'https://localhost/rest/V1/products?fields=items%5Bsku%5D&searchCriteria%5Bpage_size%5D=1&searchCriteria%5Bfilter_groups%5D%5B0%5D%5Bfilters%5D%5B0%5D%5Bfield%5D=entity_id&searchCriteria%5Bfilter_groups%5D%5B0%5D%5Bfilters%5D%5B0%5D%5Bvalue%5D=88826&searchCriteria%5Bfilter_groups%5D%5B0%5D%5Bfilters%5D%5B0%5D%5Bcondition_type%5D=eq',
    method: 'GET'
  }, oauthData)

  assertEquals(signatureBase, "GET&https%3A%2F%2Flocalhost%2Frest%2FV1%2Fproducts&fields%3Ditems%255Bsku%255D%26oauth_consumer_key%3Dabcd%26oauth_nonce%3D6MDdBa31nmI%26oauth_signature_method%3DPLAINTEXT%26oauth_timestamp%3D1462028641%26oauth_token%3Dijkl%26oauth_version%3D1.0%26searchCriteria%255Bfilter_groups%255D%255B0%255D%255Bfilters%255D%255B0%255D%255Bcondition_type%255D%3Deq%26searchCriteria%255Bfilter_groups%255D%255B0%255D%255Bfilters%255D%255B0%255D%255Bfield%255D%3Dentity_id%26searchCriteria%255Bfilter_groups%255D%255B0%255D%255Bfilters%255D%255B0%255D%255Bvalue%255D%3D88826%26searchCriteria%255Bpage_size%255D%3D1")
})

Deno.test("getSignatureBaseString() to work correctly with query string containing pluses", async () => {
  const signatureBase = await oauthSignatureTest.getSignatureBaseString({
    url: 'https://localhost?test=+123+',
    method: 'GET'
  }, oauthData)

  assertEquals(signatureBase, "GET&https%3A%2F%2Flocalhost%2F&oauth_consumer_key%3Dabcd%26oauth_nonce%3D6MDdBa31nmI%26oauth_signature_method%3DPLAINTEXT%26oauth_timestamp%3D1462028641%26oauth_token%3Dijkl%26oauth_version%3D1.0%26test%3D%2520123%2520")
})

Deno.test("getSignatureBaseString() to work correctly with query string containing pluses", async () => {
  const signatureBase = await oauthSignatureTest.getSignatureBaseString({
    url: 'https://localhost/rest/V1/products/SMKDCC2+',
    method: 'GET'
  }, oauthData)

  assertEquals(signatureBase, "GET&https%3A%2F%2Flocalhost%2Frest%2FV1%2Fproducts%2FSMKDCC2%2B&oauth_consumer_key%3Dabcd%26oauth_nonce%3D6MDdBa31nmI%26oauth_signature_method%3DPLAINTEXT%26oauth_timestamp%3D1462028641%26oauth_token%3Dijkl%26oauth_version%3D1.0")
})
