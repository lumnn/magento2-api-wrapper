/**
 * Flattens object/array to an object which keys are nested using square brackets
 */
export function flatten(
  data: { [name: string]: any },
): { [name: string]: any } {
  const flatten = flattenToArray(data)

  return Object.fromEntries(flatten.map((v) => [
    Array.isArray(v[0])
      ? (v[0].shift() + (v[0].map((p) => `[${p}]`)).join(""))
      : v[0],
    v[1],
  ]))
}

function flattenToArray(
  data: { [name: string]: any },
): [string | string[], any][] {
  const flat: [string | string[], any][] = []

  for (const name in data) {
    const value = data[name]

    // copy non nested values
    if (typeof value !== "object") {
      flat.push([name, value])
      continue
    }

    const flatValues = flattenToArray(value)
      .map((v) => {
        return [
          Array.isArray(v[0]) ? [name, ...v[0]] : [name, v[0]],
          v[1],
        ] as [string[], any]
      })

    flat.push(...flatValues)
  }

  return flat
}
