import { OAuth } from "./OAuth.ts"
import { hmacSha256 } from "./hash/hmacSha256.ts";
import { flatten } from "./util/flatten.ts"

export class Magento2Api {
  oauth: undefined|OAuth
  options: MagentoApiOptions
  requestMiddleware: ((request: Request) => any)[]
  responseMiddleware: ((response: Response, error?: Error) => any)[]

  constructor(options: MagentoApiOptions) {
    this.options = options
    this.requestMiddleware = []
    this.responseMiddleware = []

    if (options.url.endsWith("/")) {
      options.url = options.url.replace(/\/+$/, '')
    }

    if (options.consumerKey && options.consumerSecret) {
      this.oauth = new MagentoOAuth({
        consumer: { key: options.consumerKey, secret: options.consumerSecret },
        signatureMethod: 'HMAC-SHA256',
        hashMethods: {
          "HMAC-SHA256": hmacSha256
        }
      })
    }
  }

  async request(method: string, path: string, data?: any, options?: RequestOptions): Promise<Response> {
    const request = await this.buildRequest(method, path, data, options)
    return this.fetch(request)
  }

  /**
   * Internal method that could be overriden to decorate more response / reqest / add exception etc.
   */
  fetch(request: Request): Promise<Response> {
    return fetch(request)
  }

  async buildRequest(method: string, path: string, data?: any, options?: RequestOptions): Promise<Request> {
    if (path[0] !== '/') {
      const version = this.options.apiVersion || 1
      let storeCode = options?.storeCode || this.options.storeCode || ''
      if (storeCode) {
        storeCode += "/"
      }
      path = `/rest/${storeCode}V${version}/${path}`
    }

    const query = options?.params
      ? Object.entries(flatten(options.params)).map(p => `${encodeURIComponent(p[0])}=${encodeURIComponent(p[1])}`).join('&')
      : null

    const url = this.options.url + path + (query ? `?${query}` : '')
    const body = data ? JSON.stringify(data) : null

    const request = new Request(url, {
      method,
      ...options,
      headers: {
        "Content-Type": "application/json",
        ...(options?.headers ? options.headers : {})
      },
      body
    })

    if (this.oauth && this.options.accessToken && this.options.tokenSecret) {
      await this.oauth.authRequest(request, { key: this.options.accessToken, secret: this.options.tokenSecret })
    }

    return request
  }

  async $<T>(method: string, path: string, data: any, options?: RequestOptions): Promise<T> { return (await this.request(method, path, data, options)).json() as Promise<T> }
  async $get<T>(path: string, options?: RequestOptions): Promise<T> { return (await this.request('get', path, null, options)).json() as Promise<T> }
  async $post<T>(path: string, data: any, options?: RequestOptions): Promise<T> { return (await this.request('post', path, data, options)).json() as Promise<T> }
  async $patch<T>(path: string, data: any, options?: RequestOptions): Promise<T> { return (await this.request('patch', path, data, options)).json() as Promise<T> }
  async $put<T>(path: string, data: any, options?: RequestOptions): Promise<T> { return (await this.request('put', path, data, options)).json() as Promise<T> }
  async $delete<T>(path: string, options?: RequestOptions): Promise<T> { return (await this.request('delete', path, null, options)).json() as Promise<T> }
}

export type MagentoApiOptions = {
  url: string,
  storeCode?: string,
  apiVersion?: number,
  consumerKey?: string,
  consumerSecret?: string,
  accessToken?: string,
  tokenSecret?: string,
}

type RequestOptions = RequestInit & {
  params?: any,
  storeCode?: string,
}

class MagentoOAuth extends OAuth {
  // avoids sign requests issues when SKU for example contains special characters
  constructRequestUrl(request: Pick<Request, "url">): string {
    return decodeURIComponent(super.constructRequestUrl(request))
  }
}
