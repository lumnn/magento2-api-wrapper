# Magento 2 API

[![License: MIT](https://img.shields.io/npm/l/magento2-api-wrapper)](LICENSE)
[![NPM](https://img.shields.io/npm/v/magento2-api-wrapper)](https://www.npmjs.com/package/magento2-api-wrapper)
[![Dependencies](https://img.shields.io/librariesio/release/npm/magento2-api-wrapper)](#)
[![Builds](https://gitlab.com/lumnn/magento2-api-wrapper/badges/master/pipeline.svg)](#)
[![Coverage](https://gitlab.com/lumnn/magento2-api-wrapper/badges/master/coverage.svg)](#)

> Small Magento 2 API client that's ready to use. Works in browsers, node and
> Deno.

- Dependency free
- Works in Browser, node, Deno
- As minimal as it can get

## Node.js

### Install

```sh
npm install magento2-api-wrapper
```

or

```sh
npm jsr install @lumnn/magento2-api
```

### Usage

**As a guest**

```js
import { Magento2Api } from "magento2-api-wrapper"
// or
// const Magento2Api = require('magento2-api-wrapper')

var consumer = new Magento2Api({ baseUrl: "https://localhost" })

consumer.$get("directory/countries")
  .then((data) => console.log)

// or in async functions
var countries = await customer.$get("directory/countries")
```

**As a admin/customer**

```js
// Api Keys: Magento Admin > System > Extensions > Integration
var admin = new Magento2Api({
  url: "https://localhost",
  consumerKey: "xxx",
  consumerSecret: "xxx",
  accessToken: "xxx",
  tokenSecret: "xxx",
})

admin.$get("products", {
  params: {
    searchCriteria: {
      currentPage: 1,
      pageSize: 1,
    },
  },
})
  .then((data) => console.log)
```

**Responses:** Successfull response for methods starting with `$` returns plain Magento data. `request` method returns whole response data (including status, headers, etc.)

## NPM

```ts
import { Magento2Api } from "magento2-api-wrapper"
```

## Deno

Above examples should be pretty much similar only difference is in how module is
imported

```ts
import { Magento2Api } from "@lumnn/magento2-api"
```

## Methods / Properties

Basic request method to trigger any kind of request

- `.request(method: string, path: string, data: any, options?: RequestOptions): Promise`

Additionally following helper methods are available that simplify the process of
getting JSON data and adding types to responses (supports generics)

- `.$get(url: string, options?: RequestOptions): Promise`
- `.$delete(url: string, options?: RequestOptions): Promise`
- `.$post(url: string, data: any, options?: RequestOptions): Promise`
- `.$put(url: string, data: any, options?: RequestOptions): Promise`
- `.$patch(url: string, data: any, options?: RequestOptions): Promise`

## Options

**Constructor Options**

- `api.url`: `string` - **required** - a baseUrl for magento instace
- `api.consumerKey`: `string` - _(optional)_ - for authentication
- `api.consumerSecret`: `string` - _(optional)_ - for authentication
- `api.accessToken`: `string` - _(optional)_ - for authentication
- `api.tokenSecret`: `string` - _(optional)_ - for authentication

**Method options**

When executing any of the methods like `.get`, `.post` you may use extra config
options on top of regular Request config:

- `params`: `object` - an object for easier GET parameter building
- `storeCode`: `string` - setting storeCode will change base url so it's like
  `https://example.org/rest/{storeCode}/V1/`

## Useful Examples

### Allowing self-signed certificate

Deno: `--unsafely-ignore-certificate-errors=localhost`.
Node: `NODE_TLS_REJECT_UNAUTHORIZED=0`

## Run tests

```sh
deno test
```
