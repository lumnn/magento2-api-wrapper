// ex. scripts/build_npm.ts
import { build, emptyDir } from "https://deno.land/x/dnt/mod.ts"

await emptyDir("./npm")

await build({
  entryPoints: ["./Magento2Api.ts"],
  outDir: "./npm",
  shims: {
    // see JS docs for overview and more options
    deno: true,
  },
  package: {
    // package.json properties
    name: "magento2-api-wrapper",
    author: "lumnn",
    version: Deno.args[0],
    description:
      "Minimal Magento 2 API library. Both node and browser compatible",
    license: "MIT",
    keywords: [
      "magento2",
      "api",
      "ecommerce",
      "node",
      "javascript",
      "browser",
    ],
    repository: {
      type: "git",
      url: "git+https://gitlab.com/lumnn/magento2-api-wrapper.git",
    },
    bugs: {
      url: "https://gitlab.com/lumnn/magento2-api-wrapper/issues",
    },
  },
  postBuild() {
    // steps to run after building and before running the tests
    Deno.copyFileSync("LICENSE", "npm/LICENSE")
    Deno.copyFileSync("README.md", "npm/README.md")
  },
})
