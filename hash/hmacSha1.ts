import { createHmacHasher } from "./createHmacHasher.ts"

export const hmacSha1 = createHmacHasher("SHA-1")
