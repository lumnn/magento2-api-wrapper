export function createHmacHasher(hash: string) {
  return async function hmacHasher(
    key: string,
    content: string,
  ): Promise<string> {
    // encoder to convert string to Uint8Array
    const enc = new TextEncoder()
    const algo = { name: "HMAC", hash }

    const cryptKey = await crypto.subtle.importKey(
      "raw",
      enc.encode(key),
      algo,
      false,
      ["sign", "verify"],
    )

    const signature = await crypto.subtle.sign(
      algo,
      cryptKey,
      enc.encode(content),
    )

    const b = new Uint8Array(signature)
    // base64 digest
    return btoa(String.fromCharCode(...b))
  }
}
