import { createHmacHasher } from "./createHmacHasher.ts"

export const hmacSha256 = createHmacHasher("SHA-256")
